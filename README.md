# Deploy Qt5 Applications compiled using MXE

This script can be used to deploy Qt5 applications which have been
compiled using MXE. It does the following:

* If required, scan the application's QML sources for dependencies.
* Copy platform plugins and - optionally - requested other plugin types.
* Deploy any DLLs from the MXE installation to the application directory.

After the script finished, the application should be self-contained, i.e.
the directory can be copied as-is to a Windows system and the contained
application be executed.


## Requirements

The script is written using Python 3, so make sure it is installed. Only
core modules of Python are used, so no additional modules need to be
installed.

In addition, it assumes that the build is done using an
[MXE](mxe.cc) build environment. Other cross-compile environments
might work as well, but are not tested.


## Example

Let's assume we have an application in `/MyApp` and an MXE installation
in `/opt/mxe-x64-shared`. Then we could build and deploy the application
like this:

```bash
cd /MyApp
mkdir build
cd build
/opt/mxe-x64-shared/usr/x86_64-w64-mingw32.shared/qt5/bin/qmake ..
make
cd ..
mkdir deploy/MyApp
cp build/release/MyApp.exe deploy/MyApp/

# Make the application self-contained. In addition to the
# `platform` plugins, also copy the `imageformats` ones:
xwindeployqt \
    --toolchain-root /opt/mxe-x64-shared/  \
    --toolchain-arch x86_64-w64-mingw32.shared \
    --qml-dir . \
    --plugins imageformats \
    deploy/MyApp/
```


## Tips

### Reducing the size of the deployment folder

If you want to create minimal deployment folders, you might want to consider
excluding certain plugins. For example, if your app requires the `imageformas`
plugins, the script will also deploy the SVG plugin, which in turn
pulls in the `Svg` module and that in turn the `Widgets` one. This can quickly
sum up. Hence, an easy way to reduce the overall size of the deployed
application is to explicitly exclude plugins you do not need. For example, if
we do not need SVG support, we can skip deployment of the SVG plugin (and
all its dependencies) by using the `--exclude-plugin` option:

```bash
xwindeployqt \
    --toolchain-root /opt/mxe-x64-shared/  \
    --toolchain-arch x86_64-w64-mingw32.shared \
    --qml-dir . \
    --plugins imageformats \
    --exclude-plugin qsvg.dll \
    deploy/MyApp/
```
